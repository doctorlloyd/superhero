package com.example.superhero.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.superhero.R
import com.example.superhero.models.Hero
import com.example.superhero.ui.Profile

class SearchAdapter(private val context: Context, private val heros: List<Hero>) :
    RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var name: TextView = itemView.findViewById(R.id.name)
        var power: TextView = itemView.findViewById(R.id.power)
        var work: TextView = itemView.findViewById(R.id.work)
        var fullName: TextView = itemView.findViewById(R.id.full_name)
        var connections: TextView = itemView.findViewById(R.id.connections)
        var heroCardView: CardView = itemView.findViewById(R.id.heroCardView)


        init {
            heroCardView.setOnClickListener { view: View ->
                val intent = Intent(context, Profile::class.java)
                intent.putExtra("hero", heros[adapterPosition])
                view.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_tab, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return heros.size
    }

    override fun onBindViewHolder(holder: SearchAdapter.ViewHolder, position: Int) {
        val hero = heros[position]
        holder.name.text = hero.name
        holder.power.text = hero.powerstats.intelligence
        holder.work.text = hero.work.occupation
        holder.fullName.text = hero.biography.full_name
        holder.connections.text = hero.connections.group_affiliation


    }

}