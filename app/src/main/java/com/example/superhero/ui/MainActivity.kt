package com.example.superhero.ui

import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.superhero.R
import com.example.superhero.adapters.SearchAdapter
import com.example.superhero.models.Hero
import com.example.superhero.models.SearchResult
import com.example.superhero.network.ServiceManager
import com.example.superhero.ui.support.DialogLayout
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private var routes: ServiceManager = ServiceManager(this)
    private lateinit var searchAdapter: SearchAdapter
    private var numeric = true
    private lateinit var dialog: DialogLayout
    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dialog = DialogLayout(this)
        create()

        layoutManager = LinearLayoutManager(this)
        search_recycler_view.layoutManager = layoutManager

        swipeToRefresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                this,
                R.color.colorPrimary
            )
        )
        swipeToRefresh.setColorSchemeColors(Color.WHITE)

        swipeToRefresh.setOnRefreshListener {
            getHeroByID(70)
            swipeToRefresh.isRefreshing = false
        }

        landingVideo.setVideoURI(Uri.parse("android.resource://$packageName/${R.raw.home_screen_video}"))
        landingVideo.requestFocus()
        landingVideo.setMediaController(MediaController(this))
        landingVideo.start()

        search_super_hero.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                numeric = query.matches("-?\\d+(\\.\\d+)?".toRegex())
                if (numeric) {
                    getHeroByID(query.toInt())
                } else {
                    getHeroByName(query)
                }
                return false
            }
        })
    }


    private fun getHeroByID(heroID: Int) {
        if (!dialog.active())
            dialog.show()
        routes.getUserByID(heroID).enqueue(object : Callback<Hero> {
            override fun onResponse(call: Call<Hero>, response: Response<Hero>) {
                if (response.isSuccessful) {
                    val hero = response.body()!!
                    main_layout.visibility = View.VISIBLE
                    swipeToRefresh.visibility = View.GONE

                    dialog.hideDialog()

                    val intent = Intent(this@MainActivity, Profile::class.java)
                    intent.putExtra("hero", hero)
                    startActivity(intent)

                } else {
                    display_response.text = getString(R.string.errorOnCall)
                    dialog.hideDialog()
                }
            }

            override fun onFailure(call: Call<Hero>, t: Throwable) {

            }
        })
    }


    private fun getHeroByName(heroName: String) {
        if (!dialog.active())
            dialog.show()
        routes.getUserByName(heroName).enqueue(object : Callback<SearchResult> {
            override fun onResponse(call: Call<SearchResult>, response: Response<SearchResult>) {
                if (response.isSuccessful) {
                    val results = response.body()!!
                    if(results.results.isNotEmpty()) {
                        Thread(Runnable {
                            try {
                                val url = URL(results.results[0].image.url)
                                val bitmap =
                                    BitmapFactory.decodeStream(
                                        url.openConnection().getInputStream()
                                    )
                                val image: Drawable =
                                    BitmapDrawable(resources, bitmap)
                                search_recycler_view.background = image

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }).start()

                        searchAdapter = SearchAdapter(this@MainActivity, results.results)
                        search_recycler_view.adapter = searchAdapter

                        main_layout.visibility = View.GONE
                        swipeToRefresh.visibility = View.VISIBLE

                        landingVideo.stopPlayback()
                    }
                } else {
                    display_response.text = getString(R.string.errorOnCall)
                }
                dialog.hideDialog()
            }

            override fun onFailure(call: Call<SearchResult>, t: Throwable) {

            }
        })
    }

    private fun create() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.createDialog()
        }
    }

    override fun onBackPressed() {
        counter++
        if (counter == 1) {
            main_layout.visibility = View.VISIBLE
            swipeToRefresh.visibility = View.GONE
            Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_LONG).show()
        } else
            exitProcess(0)
    }
}
