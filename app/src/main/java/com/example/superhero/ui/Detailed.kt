package com.example.superhero.ui

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.superhero.R
import com.example.superhero.locals.Preference
import com.example.superhero.models.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.appearance.*
import kotlinx.android.synthetic.main.biography.*
import kotlinx.android.synthetic.main.connections.*
import kotlinx.android.synthetic.main.image.*
import kotlinx.android.synthetic.main.powerstats.*
import kotlinx.android.synthetic.main.work.*
import java.lang.Exception

class Detailed : AppCompatActivity() {
    private lateinit var hero: Any
    private lateinit var preference: Preference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)
        preference = Preference(this)
        try {
            hero = intent.getSerializableExtra("hero") as Any
            when (hero) {
                is Appearance -> {
                    appearance_layout.visibility = View.VISIBLE
                    title = getString(R.string.appearance)
                    appearance(hero as Appearance)
                }
                is Biography -> {
                    biography_layout.visibility = View.VISIBLE
                    title = getString(R.string.biography)
                    biography(hero as Biography)
                }
                is Connections -> {
                    connections_layout.visibility = View.VISIBLE
                    title = getString(R.string.connections)
                    connections(hero as Connections)
                }
                is Image -> {
                    image_layout.visibility = View.VISIBLE
                    title = getString(R.string.image)
                    image(hero as Image)
                }
                is Powerstats -> {
                    powerstats_layout.visibility = View.VISIBLE
                    title = getString(R.string.powerstats)
                    powerstats(hero as Powerstats)
                }
                is Work -> {
                    work_layout.visibility = View.VISIBLE
                    title = getString(R.string.work)
                    work(hero as Work)
                }
            }
        } catch (e: Exception) {
            Log.d(ContentValues.TAG, "Error: " + e.localizedMessage)
        }
    }

    private fun appearance(appearance: Appearance) {
        appearance_eye_color.text = appearance.eye_color
        appearance_gender.text = appearance.gender
        appearance_hair_color.text = appearance.hair_color
        appearance_height.text = appearance.height[0]
        appearance_race.text = appearance.race
        appearance_weight.text = appearance.weight[0]

        appearance_name.text = preference.getName()
    }

    private fun biography(biography: Biography) {
        biography_aliases.text = biography.aliases[0]
        biography_alignment.text = biography.alignment
        biography_alter_egos.text = biography.alter_egos
        biography_first_appearance.text = biography.first_appearance
        biography_full_name.text = biography.full_name
        biography_place_of_birth.text = biography.place_of_birth
        biography_publisher.text = biography.publisher

        biography_name.text = preference.getName()
    }

    private fun connections(connections: Connections) {
        connections_group_affiliation.text = connections.group_affiliation
        connections_relatives.text = connections.relatives

        connections_name.text = preference.getName()
    }

    private fun image(image: Image) {
        if (image.url.isNotEmpty()) {
            Picasso.get().load(image.url).into(image_layout)
        }
    }

    private fun powerstats(powerstats: Powerstats) {
        powerstats_combat.text = powerstats.combat
        powerstats_durability.text = powerstats.durability
        powerstats_intelligence.text = powerstats.intelligence
        powerstats_power.text = powerstats.power
        powerstats_speed.text = powerstats.speed
        powerstats_strength.text = powerstats.strength

        powerstats_name.text = preference.getName()
    }

    private fun work(work: Work) {
        work_base.text = work.base
        work_occupation.text = work.occupation

        work_name.text = preference.getName()
    }
}
