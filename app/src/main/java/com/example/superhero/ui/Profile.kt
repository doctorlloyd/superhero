package com.example.superhero.ui

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.superhero.R
import com.example.superhero.locals.Preference
import com.example.superhero.models.Hero
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile.*
import java.lang.Exception

class Profile : AppCompatActivity() {
    private lateinit var preference: Preference
    private lateinit var hero: Hero
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)
        preference = Preference(this)
        try {
            hero = intent.getSerializableExtra("hero") as Hero

            if (hero.image.url.isNotEmpty()) {
                Picasso.get().load(hero.image.url).into(avatar)
            }

            preference.setName(hero.name)

            hero_name.text = hero.name
            speed.text = hero.powerstats.speed
            combat.text = hero.powerstats.combat
            power.text = hero.powerstats.power

            val id = "${getString(R.string.super_hero_id)} ${hero.id}"
            hero_id.text = id
            val res = "${getString(R.string.response_time)} ${hero.response}"
            response_time.text = res
            val name = "${getString(R.string.hero_name)} ${hero.biography.full_name}"
            full_name_hero.text = name

            val intent = Intent(this, Detailed::class.java)
            appearancebtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.appearance
                )
                startActivity(intent)
            }

            workbtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.work
                )
                startActivity(intent)
            }

            biographybtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.biography
                )
                startActivity(intent)
            }

            connectionsbtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.connections
                )
                startActivity(intent)
            }

            powerstatsbtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.powerstats
                )
                startActivity(intent)
            }

            imagebtn.setOnClickListener {
                intent.putExtra(
                    "hero",
                    hero.image
                )
                startActivity(intent)
            }
        } catch (e: Exception) {
            Log.d(ContentValues.TAG, "Error: " + e.localizedMessage)
        }
    }
}
