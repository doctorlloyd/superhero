
class HttpStatus {
    val Accepted = 202
    val BadGateway = 502
    val BadRequest = 400
    val Conflict = 409
    val Continue = 100
    val Created = 201
    val ExpectationFailed = 417
    val FailedDependency = 424
    val Forbidden = 403
    val GatewayTimeout = 504
    val Gone = 410
    val HTTPVersionNotSupported = 505
    val InsufficientStorage = 507
    val ServerError = 500
    val LengthRequired = 411
    val Locked = 423
    val MethodNotAllowed = 405
    val MovedPermanently = 301
    val MovedTemporarily = 302
    val MutlipleChoices = 300
    val MultiStatusorPartialUpdateOK = 207
    val NonAuthoritativeInformation = 203
    val NotAcceptable = 406
    val NotFound = 404
    val NotImplemented = 501
    val NotModified = 304
    val NoContent = 204
    val OK = 200
    val PartialContent = 206
    val PaymentRequired = 402
    val PreconditionFailed = 412
    val Processing = 102
    val ProxyAuthenticationRequired = 407
    val RequestedRangeNotSatisfiable = 416
    val RequestTimeout = 408
    val RequestEntityTooLarge = 413
    val RequestURITooLong = 414
    val ResetContent = 205
    val SeeOther = 303
    val ServiceUnavailable = 503
    val SwitchingProtocols = 101
    val TemporaryRedirect = 307
    val Unauthorized = 401
    val UnprocessableEntity = 422
    val UnsupportedMediaType = 415
    val UseProxy = 305

    private val httpStatusCodes = object : HashMap<Int, String>() {
        init {
            put(Accepted, "Accepted")
            put(BadGateway, "Bad Gateway")
            put(BadRequest, "Bad Request")
            put(Conflict, "Conflict")
            put(Continue, "Continue")
            put(Created, "Created")
            put(ExpectationFailed, "Expectation Failed")
            put(FailedDependency, "Failed Dependency")
            put(Forbidden, "Forbidden")
            put(GatewayTimeout, "Gateway Timeout")
            put(Gone, "Gone")
            put(HTTPVersionNotSupported, "HTTP Version Not Supported")
            put(InsufficientStorage, "Insufficient Storage")
            put(ServerError, "Server Error")
            put(LengthRequired, "Length Required")
            put(Locked, "Locked")
            put(MethodNotAllowed, "Method Not Allowed")
            put(MovedPermanently, "Moved Permanently")
            put(MovedTemporarily, "Moved Temporarily")
            put(MutlipleChoices, "Multiple Choices")
            put(MultiStatusorPartialUpdateOK, "Multi-Status or Partial Update OK")
            put(NonAuthoritativeInformation, "Non Authoritative Information")
            put(NotAcceptable, "Not Acceptable")
            put(NotFound, "Not Found")
            put(NotImplemented, "Not Implemented")
            put(NotModified, "Not Modified")
            put(NoContent, "No Content")
            put(OK, "OK")
            put(PartialContent, "Partial Content")
            put(PaymentRequired, "Payment Required")
            put(PreconditionFailed, "Precondition Failed")
            put(Processing, "Processing")
            put(ProxyAuthenticationRequired, "Proxy Authentication Required")
            put(RequestedRangeNotSatisfiable, "Requested Range Not Satisfiable")
            put(RequestTimeout, "Request Timeout")
            put(RequestEntityTooLarge, "Request Entity Too Large")
            put(RequestURITooLong, "Request-URI Too Long")
            put(ResetContent, "Reset Content")
            put(SeeOther, "See Other")
            put(ServiceUnavailable, "Service Unavailable")
            put(SwitchingProtocols, "Switching Protocols")
            put(TemporaryRedirect, "Temporary Redirect")
            put(Unauthorized, "Unauthorized")
            put(UnprocessableEntity, "Unprocessed Entity")
            put(UnsupportedMediaType, "Unsupported Media Type")
            put(UseProxy, "Use Proxy")
        }
    }

    fun getStatusMessage(code: Int): String? {
        return httpStatusCodes[code]
    }
}