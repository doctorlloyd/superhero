package com.example.superhero.network

import android.content.Context
import com.example.superhero.models.Hero
import com.example.superhero.models.SearchResult
import com.example.superhero.network.interceptors.LoggingInterceptor
import com.github.simonpercic.oklog3.OkLogInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ServiceManager(private var context: Context) {
    private val headers = HashMap<String, String>()
    private val uriPattern = "https://superheroapi.com/api/2745370772166409/"
    
    private fun getHttpClient(): OkHttpClient {
        return getHttpClientBuilder()
            .connectTimeout(500, TimeUnit.SECONDS)
            .writeTimeout(500, TimeUnit.SECONDS)
            .readTimeout(500, TimeUnit.SECONDS)
            .build()
    }

    private fun getHttpClientBuilder(): OkHttpClient.Builder {
        val httpClientBuilder = OkHttpClient.Builder()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClientBuilder.addInterceptor(LoggingInterceptor())
        httpClientBuilder.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
        httpClientBuilder.addInterceptor(
            OkLogInterceptor.builder()
                .withRequestHeaders(true)
                .withRequestBody(true)
                .withRequestBodyState(true)
                .withResponseHeaders(true)
                .withResponseBodyState(true)
                .withRequestContentType(true)
                .withRequestContentLength(true)
                .withResponseMessage(true)
                .withAllLogData()
                .build()
        )
        return httpClientBuilder
    }

    private fun getService(): Routes {
        return getRetrofit().create(Routes::class.java)
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(uriPattern)
            .client(getHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getUserByID(userId: Int): Call<Hero> {
        return getService().getUserByID(userId)
    }

    fun getUserByName(userName: String): Call<SearchResult> {
        return getService().getUserByName(userName)
    }

    fun getUserPowerStats(id:String): Call<SearchResult> {
        return getService().getUserPowerStats(id)
    }

    fun getUserBiography(id:String): Call<SearchResult> {
        return getService().getUserBiography(id)
    }

    fun getUserAppearance(id:String): Call<SearchResult> {
        return getService().getUserAppearance(id)
    }

    fun getUserWork(id:String): Call<SearchResult> {
        return getService().getUserWork(id)
    }

    fun getUserImage(id:String): Call<SearchResult> {
        return getService().getUserImage(id)
    }

    fun getUserConnections(id:String): Call<SearchResult> {
        return getService().getUserConnections(id)
    }
}