package com.example.superhero.network

import com.example.superhero.models.Hero
import com.example.superhero.models.SearchResult
import retrofit2.Call
import retrofit2.http.*

interface Routes {

    @GET("{userID}/")
    fun getUserByID(
        @Path("userID") userID: Int
    ): Call<Hero>

    @GET("search/{userName}/")
    fun getUserByName(
        @Path("userName") userName: String
    ): Call<SearchResult>

    @GET("{id}/powerstats/")
    fun getUserPowerStats(
        @Path("id") id: String
    ): Call<SearchResult>

    @GET("{id}/biography/")
    fun getUserBiography(
        @Path("id") id: String
    ): Call<SearchResult>

    @GET("{id}/appearance/")
    fun getUserAppearance(
        @Path("id") id: String
    ): Call<SearchResult>

    @GET("{id}/work/")
    fun getUserWork(
        @Path("id") id: String
    ): Call<SearchResult>

    @GET("{id}/connections/")
    fun getUserConnections(
        @Path("id") id: String
    ): Call<SearchResult>

    @GET("{id}/image/")
    fun getUserImage(
        @Path("id") id: String
    ): Call<SearchResult>

}