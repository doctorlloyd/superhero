package com.example.superhero.models

import java.io.Serializable

data class Work(
    val base: String,
    val occupation: String
):Serializable