package com.example.superhero.models

import java.io.Serializable

data class Image(
    val url: String
): Serializable