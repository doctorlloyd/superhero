package com.example.superhero.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Connections(
    @SerializedName("group-affiliation")
    val group_affiliation: String,
    val relatives: String
): Serializable