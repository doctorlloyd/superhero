package com.example.superhero.models

import java.io.Serializable

data class Powerstats(
    val combat: String,
    val durability: String,
    val intelligence: String,
    val power: String,
    val speed: String,
    val strength: String
): Serializable