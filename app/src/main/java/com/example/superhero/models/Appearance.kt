package com.example.superhero.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Appearance(
    @SerializedName("eye-color")
    val eye_color: String,
    val gender: String,
    @SerializedName("hair-color")
    val hair_color: String,
    val height: List<String>,
    val race: String,
    val weight: List<String>
): Serializable