package com.example.superhero.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchResult (

	@SerializedName("response") val response : String,
	@SerializedName("results-for") val results_for : String,
	@SerializedName("results") val results : List<Hero>
): Serializable