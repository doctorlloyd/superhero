package com.example.superhero.locals

import android.content.Context
import android.content.SharedPreferences

class Preference(context: Context) {

    val KEY = "KEY"
    val HERO_NAME = "NAME"

    val preference: SharedPreferences =
        context.getSharedPreferences(KEY, Context.MODE_PRIVATE)

    fun setName(name: String) {
        val editor = preference.edit()
        editor.putString(HERO_NAME, name)
        editor.apply()
    }

    fun getName(): String {
        return preference.getString(HERO_NAME, HERO_NAME)!!
    }
}